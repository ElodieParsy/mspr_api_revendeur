-- --------------------------------------------------------
-- Hôte :                        10.0.0.4
-- Version du serveur:           8.0.28 - MySQL Community Server - GPL
-- SE du serveur:                Linux
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour msprtest
CREATE DATABASE IF NOT EXISTS `msprtest` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `msprtest`;

-- Listage de la structure de la table msprtest. Prospects
CREATE TABLE IF NOT EXISTS `Prospects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `crmId` int DEFAULT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `lastname` varchar(50) NOT NULL DEFAULT '0',
  `company` varchar(50) NOT NULL DEFAULT '0',
  `mail` varchar(50) NOT NULL DEFAULT '0',
  `password` longtext NOT NULL,
  `auth_token` longtext,
  `revoke_jwt` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table msprtest.Prospects : ~0 rows (environ)
/*!40000 ALTER TABLE `Prospects` DISABLE KEYS */;
INSERT INTO `Prospects` (`id`, `crmId`, `name`, `lastname`, `company`, `mail`, `password`, `auth_token`, `revoke_jwt`) VALUES
	(1, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', '95unX9rlywLhaBKsGVo9EA', NULL),
	(2, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', 'qKXhZzX5Pz79Lh5L7iDHCw', NULL),
	(3, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(4, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(5, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(6, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(7, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(8, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(9, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(10, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(11, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(12, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(13, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(14, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(15, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(16, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(17, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(18, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(19, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(20, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0),
	(21, NULL, 'test', 'elo', 'parsy', 'elodie.parsy@epsi.fr', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', NULL, 0);
/*!40000 ALTER TABLE `Prospects` ENABLE KEYS */;

-- Listage de la structure de la table msprtest. UtilisateurWebshop
CREATE TABLE IF NOT EXISTS `UtilisateurWebshop` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mail` varchar(50) NOT NULL DEFAULT '0',
  `password` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table msprtest.UtilisateurWebshop : ~0 rows (environ)
/*!40000 ALTER TABLE `UtilisateurWebshop` DISABLE KEYS */;
INSERT INTO `UtilisateurWebshop` (`id`, `mail`, `password`) VALUES
	(1, 'test_user@example.com', '8f9afb58880507ec875fd077ed76abd4eeeff81e615648d8b8b00376d076b6e5eac0f98420341f61b07cb13f7540ae52c906014fd4412dcf56aeae8cfea2c005');
/*!40000 ALTER TABLE `UtilisateurWebshop` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

#!/usr/bin/env python
#-*- coding: utf-8 -*-

from flask import Flask
from classes.Functions import Functions
from classes.Router import Router
from flaskext.mysql import MySQL
from datetime import timedelta
from flask_swagger_ui import get_swaggerui_blueprint
import sys


_f = Functions()
configuration = _f.config_yaml()

app = Flask(__name__)
app.config['DEBUG'] = True

mysql = MySQL()

### swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "MSPR - API - Prospects"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

if hasattr(sys, '_called_from_test') != True:
        app.config['MYSQL_DATABASE_HOST'] = configuration["normal"]["host"]
        app.config['MYSQL_DATABASE_PORT'] = configuration["normal"]["port"]
        app.config['MYSQL_DATABASE_USER'] = configuration["normal"]["user"]
        app.config['MYSQL_DATABASE_PASSWORD'] = configuration["normal"]["password"]
        app.config['MYSQL_DATABASE_DB'] = configuration["normal"]["db"]
        app.config['SECRET_KEY'] = configuration["normal"]["secret_key"]
else:
        app.config['MYSQL_DATABASE_HOST'] = configuration["test"]["host"]
        app.config['MYSQL_DATABASE_PORT'] = configuration["test"]["port"]
        app.config['MYSQL_DATABASE_USER'] = configuration["test"]["user"]
        app.config['MYSQL_DATABASE_PASSWORD'] = configuration["test"]["password"]
        app.config['MYSQL_DATABASE_DB'] = configuration["test"]["db"]
        app.config['SECRET_KEY'] = configuration["test"]["secret_key"]

mysql.init_app(app)
connection = mysql.connect()
cursor =connection.cursor()
db = {"connection": connection, "cursor": cursor}

router = Router(app=app, db=db, functions=_f)
router.register_routes()
app.db = db

#if __name__ == "__main__":
if hasattr(sys, '_called_from_test') != True:
        app.run(
                debug=True, 
                port=8000, 
                # host="10.0.0.4"
                host="0.0.0.0"
        )

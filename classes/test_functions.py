import pytest,os,pathlib,hashlib,datetime
from Functions import Functions
from Router import Router
from main import app
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from unittest.mock import MagicMock
from sendgrid.helpers.mail import Mail
from sendgrid import SendGridAPIClient
from unittest.mock import patch

@pytest.fixture
def functions():
    return Functions()

#
# Test que les données du qrCode se génèrent correctement
#
def test_dataQrcode(functions):
    token = "QanBwAp45xZeG8Hdy_YJzA"
    mail = "test_user@example.com"
    result = functions.dataQrcode(token,mail)
    assert result != False

#
# Test que les données du qrCode ne se génèrent pas suite à des paramètres manquants
#
def test_dataQrcodeFalse(functions):
    token = "QanBwAp45xZeG8Hdy_YJzA"
    result = functions.dataQrcode(token,None)
    assert result == False

#
# Test que le qrCode se génère correctement
#
def generateQrcode(functions):
    dataQrcode = {
        "token": "QanBwAp45xZeG8Hdy_YJzA",
        "mail": "test_user@example.com"
    }
    result = functions.generateQrcode(dataQrcode,app)
    assert result['result'] == True

#
# Test qui vérifie que le fichier yaml s'ouvre correctement et contient les champs attendus
#
def test_config_yaml_success(functions):
    config = functions.config_yaml()
    assert isinstance(config, dict)
    assert "normal" in config
    assert "test" in config   

#
# Test qui vérifie que la fonction renvoie la chaîne JSON attendue pour chaque entrée,
# et que la fonction jconverter est correctement appelée pour que l'objet datetime soit converti en chaîne.
#
def test_objectToString(functions): 
    input_dict = {"key": datetime.datetime(2022, 2, 24, 12, 0)}
    expected_output = '{\n    "key": "2022-02-24 12:00:00"\n}'
    assert functions.objectToString(input_dict) == expected_output

    # Test dictionnaire contenant des strings
    input_dict = {"key": "value"}
    expected_output = '{\n    "key": "value"\n}'
    assert functions.objectToString(input_dict) == expected_output

    # Test avec un dictionnaire contenant une liste
    input_dict = {"key": [1, 2, 3]}
    expected_output = '{\n    "key": [\n        1,\n        2,\n        3\n    ]\n}'
    assert functions.objectToString(input_dict) == expected_output

# Vérifie que la fonction renvoie bien une instance de MIMEMultipart 
# et que le message contient les en-têtes attendus.
def test_design_mail(functions):
    # Call design_mail()avec test data
    name = "John"
    lastname = "Doe"
    nomQrcode = "qrcode/Doe_Qrcode.png"
    result = functions.design_mail(name, lastname, nomQrcode)

    assert isinstance(result, str)
    assert '<h3>Bonjour {} {}</h3>'.format(name, lastname) in result
    assert '<img src="data:image/png;base64, ' in result
    


# Simule une connexion SMTP et intercepte les appels aux méthodes login() et sendmail(). 
# Vérifie que les appels ont été effectués avec les arguments attendus et que la fonction 
# Renvoie la chaîne de caractères "Mail envoyé" en cas de succès.
def test_send_mail(functions):
 
    with patch.object(SendGridAPIClient, 'send') as mock_send:
        email_receiver = 'elodie.parsy@epsi.fr'
        message = 'Test message'
        functions.send_mail(email_receiver, message)

        assert mock_send.called_once_with(Mail(
            from_email='parsyelodie@free.fr',
            to_emails=email_receiver,
            subject='Qrcode connection PayeTonKawa',
            html_content=message))

        assert functions.send_mail(email_receiver, message) == 'Mail envoyé'


from main import app
from Functions import Functions
from Router import Router
import json,hashlib,pytest



@pytest.fixture
def mockData():
    data = {
        "name":"test",
        "lastname":"elo",
        "company":"parsy",
        "mail":"elodie.parsy@epsi.fr",
        "password":"1234",
        "idProduct":2
    }
    return data

class TestApiRoutes:
    token = None
    jwtToken = None

    #
    # Test sur la route /api/prospect/register
    #
    def test_register_prospect(self,mockData):
        # Test le statut de la requête POST
        response = app.test_client().post('/api/prospect/register', data=json.dumps(mockData), content_type='application/json')
        assert response.status_code == 201

        # Test que les données renvoyées par la route
        res = json.loads(response.data.decode('utf-8'))
        assert res['name'] == mockData["name"]
        assert res["lastname"] == mockData["lastname"]
        assert res["company"] == mockData["company"]
        assert res["mail"] == mockData["mail"]

        TestApiRoutes.token = res["token"]

    #
    # Test sur la route /api/prospect/first/connexion
    #
    def test_first_connexion(self,mockData):
        test = {
            "mail":mockData["mail"],
            "token":TestApiRoutes.token
        }
        # Test le statut de la requête POST
        response = app.test_client().post('/api/prospect/first/connexion', data=json.dumps(test), content_type='application/json')
        assert response.status_code == 200
        res = json.loads(response.data.decode('utf-8'))
       
        TestApiRoutes.jwtToken = res["tokenJwt"]

    #
    # Test sur la route /api/prospect/login
    #
    def test_login(self,mockData):
        # Test le statut de la requête POST
        response = app.test_client().post('/api/prospect/login', data=json.dumps(mockData), content_type='application/json')
        assert response.status_code == 200
        
        
    #
    # Test sur la route /api/prospect/products'
    #   
    def test_products(self):
        headers = {'x-access-token': TestApiRoutes.jwtToken}
        # Test le statut de la requête get
        response = app.test_client().get('/api/prospect/products', headers=headers, content_type='application/json')
        assert response.status_code == 200

    #
    # Test sur la route /api/prospect/product/<idProduit>
    #
    def test_specific_product(self,mockData):
        headers = {'x-access-token': TestApiRoutes.jwtToken}
        # Test le statut de la requête get
        response = app.test_client().get('/api/prospect/product/'+str(mockData["idProduct"]), headers=headers, content_type='application/json')
        assert response.status_code == 200
import sys, os, json, datetime,smtplib, ssl,qrcode,image,secrets,yaml,base64
import requests, urllib3
urllib3.disable_warnings()
from flask import abort
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from os import path
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
##########################################################################
#  Script contenant les fonctions 
##########################################################################


class Functions: 
    def config_yaml(self):
        # Ouverture et lecture du fichier de configuration YAML
        try:
            _f = open('config/config.yaml', 'r')
            config = yaml.load(_f, Loader=yaml.FullLoader)
        except Exception as e:
          print("Impossible d'ouvrir le fichier" + str(e))
        return config


    def objectToString(self,element):
        return json.dumps(element,default=self.jconverter,indent=4, sort_keys=True)

    def jconverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()  


    def dataQrcode(self,token,mail):
        if token == None or mail == None  :
            return False
        else:
            dataQrcode = {
                "token": token,
                "mail":mail
            }
        
        return dataQrcode
    

    def generateQrcode(self,dataQrcode,app):
        try:
            data =  self.objectToString(dataQrcode)
            qr = qrcode.QRCode(version = 1,box_size = 10,border = 5)
            img = qrcode.make(data)
            #chemin = os.path.abspath('/qrcode/'+dataQrcode["nom"]+'_Qrcode.png')
            chemin = os.path.join(app.root_path, 'qrcode', dataQrcode["token"]+'_Qrcode.png')
            img.save(chemin)
        except Exception as e:
            print("Impossible de générer le Qrcode" + str(e))
            return False
        return {"chemin":chemin,"result":True}

    def send_mail(self, email_receiver, message):
        message = Mail(
        from_email='parsyelodie@free.fr',
        to_emails= email_receiver,
        subject='Qrcode connection PayeTonKawa',
        html_content = message)

        api_key = "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"
        try:
            sg = SendGridAPIClient(api_key)
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e)
     
        return 'Mail envoyé'

    

    def design_mail(self,name,lastname,nomQrcode):

        # on crée un e-mail
        with open(nomQrcode, 'rb') as binary_file:
            binary_file_data = binary_file.read()
            qrcodeAsB64 = base64.b64encode(binary_file_data)
        
        # on crée un texte et sa version HTML
        html = '''
            <html>
            <body>
            <h3>Bonjour '''+name+ " " +lastname +'''</h3>
            <br>
            <br>
            <p>Afin de pouvoir vous authentifiez pour la première fois à l'application mobile payetonkawa, veuillez scanner le Qrcode ci-dessous. </p>
            <br>
            <img src="data:image/png;base64, ''' + str(qrcodeAsB64.decode('utf-8')) + '''">
            </body>
            </html>
            '''

        html_mime = MIMEText(html, 'html')

        return html

    

    



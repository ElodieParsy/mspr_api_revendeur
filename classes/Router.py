import os
import sys, datetime,json,hashlib,mysql.connector, secrets, requests,qrcode,image,secrets,jwt
import requests, urllib3
urllib3.disable_warnings()
from datetime import timedelta
from flask import Flask,request,Response,abort,session
from flaskext.mysql import MySQL
from threading import Timer

if hasattr(sys, '_called_from_test'):
    from Functions import Functions
else:
    from classes.Functions import Functions



class Router: 


    def __init__(self,app=None,db=None,functions=None):

        self.app = app        
        self.functions= functions
        self.db = db
        self.secret = secrets.SystemRandom()

        """Bloc exécuté après le traitement de chaque requête Http
        -> Modification des entêtes de la réponse pour autoriser les requêtes cross-domaines (CORS) + traçage des requêtes.
        """                                      
        @app.after_request
        def afterEveryRequest(response):  
            # Modification des Headers pour autoriser les attributs spéciaux
            headers = request.headers.get('Access-Control-Request-Headers')
            if headers:
                response.headers['Access-Control-Allow-Headers'] = headers
            
            response.headers['Access-Control-Allow-Origin'] = request.environ.get('HTTP_ORIGIN')                
            response.headers['Access-Control-Allow-Credentials'] = 'true'        
            response.headers['Access-Control-Allow-Methods'] = 'GET, HEAD, POST, OPTIONS, PUT, PATCH, DELETE'  
            return response

        @app.before_request
        def check_jwt_token():  
            if "/swagger" not in request.full_path and request.path != "/api/prospect/register" and request.path != "/api/prospect/first/connexion" and request.path != "/api/prospect/login" and request.method != "OPTIONS":
                if 'x-access-token' not in request.headers:
                    abort(401, "Authentification requise.")
                else:
                    jwt_token = request.headers['x-access-token']
                    payload = jwt.decode(jwt_token, self.app.config['SECRET_KEY'], algorithms=['HS256'])
                    try:
                        if datetime.datetime.utcnow() > datetime.datetime.fromtimestamp(payload['exp']):
                            return 'Token expiré', 401
                        else:
                            sql = "SELECT id,revoke_jwt FROM Prospects WHERE id =%s and revoke_jwt =%s;"
                            val = (payload['public_id'],0)
                            self.db['cursor'].execute(sql,val)
                            user = self.db['cursor'].fetchone()

                            if user is None:
                                abort(400, "Utilisateur introuvable.")
                            elif user[1] == 1:
                                abort(401, "Droits révoqués.")
                    except jwt.ExpiredSignatureError:
                        return 'Token expiré', 401

                    

    """
        Définitions des routes du revendeur

    """
    def register_routes(self):
        app = self.app    
        db = self.db['cursor']
        db_connection = self.db['connection']
        _ = self.functions


        #
        # Enregistre un nouveau revendeur (nom,prénom,entreprise,mail,password)
        #
        @app.route('/api/prospect/register', methods=['POST'])
        def register():
           # Récupération des paramètres de la requête
            try:
                data = json.loads(request.data)
            except Exception as e:
                print(e)
                abort(400, "Les paramètres doivent être au format json")
            
            # on hash le password
            secret = hashlib.sha512(
                data['password'].encode('utf-8'), # Convert the password 
                ).hexdigest()

            # Génération du token d'authentification temporaire
            token = secrets.token_urlsafe(16)

            # Création du nouveau revendeur en base (Prospects)
            try:
                sql = "INSERT INTO Prospects (name,lastname,company,mail,password,auth_token) VALUES (%s, %s, %s, %s, %s,%s)"
                val = (data["name"],data["lastname"],data["company"],data["mail"],secret,token)
                db.execute(sql, val)
                db_connection.commit()
            except Exception as e:
                print(e)            

            # Génération du Qrcode contenant le mail et le token temporaire de l'utilisateur
            dataQrcode = _.dataQrcode(token,data["mail"])
            if dataQrcode != False:
                result = _.generateQrcode(dataQrcode,app)
           
            # Envoie par mail du Qrcode au nouveau client
            message = _.design_mail(data["name"],data["lastname"],result["chemin"])
            mail = _.send_mail(data['mail'],message)

            del data["password"]
            data["token"] = token

            return _.objectToString(data), 201
        
        
        #
        # Route première authentification à parir du Qrcode
        #
        @app.route('/api/prospect/first/connexion', methods=['POST'])
        def first_connexion():
            # Récupération des paramètres de la requête
            try:
                data = json.loads(request.data)
            except Exception as e:
                print(e)
                abort(400, "Les paramètres doivent être au format json")
            
            # Vérification de l'existance de l'utilisateur (mail et auth_token dans le Qrcode)
            sql = "SELECT id FROM Prospects WHERE mail =%s and auth_token =%s;"
            val = (data["mail"],data["token"])
            db.execute(sql,val)
            user = db.fetchone()

            if user is None:
                abort(400, "Identifiants incorrects.")
                
            # Mise à jour de la table Prospects
            sql2 = "UPDATE Prospects set auth_token =%s, revoke_jwt =%s WHERE id =%s"
            val2 = (None,0,user[0])
            db.execute(sql2, val2)
            db_connection.commit()

            # Génération token jwt
            tokenJwt = jwt.encode({
                'public_id': user[0],
                'exp' : datetime.datetime.utcnow() + timedelta(minutes = 50)
            }, app.config['SECRET_KEY'],algorithm='HS256')
  
            return {'tokenJwt': tokenJwt},200

        #
        # Route de login : mail + password
        #
        @app.route('/api/prospect/login', methods=['POST'])
        def login():
            # Récupération des paramètres de la requête
            try:
                data = json.loads(request.data)
            except Exception as e:
                print(e)
                abort(400, "Les paramètres doivent être au format json")
            
            # on hash le password
            secret = hashlib.sha512(
                data['password'].encode('utf-8'), # Convert the password 
                ).hexdigest()

            # Vérification de l'existance de l'utilisateur (mail et password) et si ses droits ne sont pas révoqués.
            sql = "SELECT id,revoke_jwt FROM Prospects WHERE mail =%s and password =%s;"
            val = (data["mail"],secret)
            db.execute(sql,val)
            user = db.fetchone()

            if user is None:
                abort(400, "Identifiants incorrects.")
            elif user[1] == 1:
                abort(401, "Droits révoqués.")

            # Génération token jwt
            tokenJwt = jwt.encode({
                'public_id': user[0],
                'exp' : datetime.datetime.utcnow() + timedelta(minutes = 50)
            }, app.config['SECRET_KEY'],algorithm='HS256')
        
            return {'tokenJwt': tokenJwt},200


        #
        # Récupère pour chaque produit : (id,nom, description,prix,couleur,stock)
        #
        @app.route('/api/prospect/products', methods=['GET'])
        def get_products():
            data = []
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products',verify=False)
               products = r.json()
            except Exception as e:
                print("Impossible de récupérer la liste des produits " + str(e))  
                abort(400)
            
            for product in products:
                data.append({
                    "id":product["id"],
                    "name":product["name"],
                    "description":product["details"]["description"],
                    "price":product["details"]["price"],
                    "color":product["details"]["color"],
                    "stock":product["stock"]
                })

            return _.objectToString(data),200


        #
        # Récupère pour un produit spécifique : (id,nom, description,prix,couleur,stock)
        # @param = idProduct
        #
        @app.route('/api/prospect/product/<idProduct>', methods=['GET'])
        def get_product(idProduct):
            data = {}
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/'+str(idProduct),verify=False)
               product = r.json()
            except Exception as e:
                print("Impossible de récupérer le produit " + str(e))  
                abort(400)
            
            data = {
                "id":product["id"],
                "name":product["name"],
                "description":product["details"]["description"],
                "price":product["details"]["price"],
                "color":product["details"]["color"],
                "stock":product["stock"]
            }
           
            return _.objectToString(data),200

        




    
